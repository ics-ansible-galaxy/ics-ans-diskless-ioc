import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("diskless_ioc")


def test_conda_installed(host):
    with host.sudo("iocuser"):
        cmd = host.run("/opt/conda/bin/conda info")
    assert cmd.rc == 0
    assert "platform : linux-ppc64" in cmd.stdout
